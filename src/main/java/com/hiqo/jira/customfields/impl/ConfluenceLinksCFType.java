/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.customfields.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.StringConverter;
import com.atlassian.jira.issue.customfields.impl.TextAreaCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.hiqo.jira.confluence.plugin.beans.LinkInfo;
import com.hiqo.jira.confluence.plugin.manager.ConfluenceLinkConfigManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Class represents custom field to work with stored confluence links.
 * 
 * @author alexander.kosko
 */
public class ConfluenceLinksCFType extends TextAreaCFType {

    private Logger logger = Logger.getLogger(ConfluenceLinksCFType.class);
    private final String LINK_PROPERTY_NAME = "name";
    private final String LINK_PROPERTY_URL = "url";
    private final String LINK_PROPERTY_SPACE = "space";
    private final String LINK_PROPERTY_TYPE = "type";

    /**
     * Public constructor
     * @param customFieldValuePersister
     * @param stringConverter
     * @param genericConfigManager 
     */
    public ConfluenceLinksCFType(CustomFieldValuePersister customFieldValuePersister, StringConverter stringConverter, GenericConfigManager genericConfigManager) {
        super(customFieldValuePersister, stringConverter, genericConfigManager);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);
        // Create and fill array of links
        List<LinkInfo> linkInfoList = new ArrayList<LinkInfo>();
        if (issue != null) {
            String fieldValue = field.getValueFromIssue(issue);
            if (fieldValue != null) {
                String[] links = fieldValue.trim().split("\n");
                if (links != null) {
                    for (String link : links) {
                        LinkInfo linkInfo = getLinkInfo(link);
                        if (linkInfo != null) {
                            linkInfoList.add(linkInfo);
                        }
                    }
                }
            }
        }
        params.put("links", linkInfoList);
        params.put("confluenceUrl", ConfluenceLinkConfigManager.getInstance().getConfluenceUrl());

        return params;
    }

    private LinkInfo getLinkInfo(String link) {
        LinkInfo result = null;
        if (link != null) {
            try {
                link = link.replaceAll(";", "\n");
                Properties props = new Properties();
                props.load(new StringReader(link));
                result = new LinkInfo(props.getProperty(LINK_PROPERTY_NAME, ""),
                        props.getProperty(LINK_PROPERTY_URL, ""),
                        props.getProperty(LINK_PROPERTY_SPACE, ""),
                        props.getProperty(LINK_PROPERTY_TYPE, ""));
            } catch (IOException ex) {
                logger.error("Cannot parse confluence link info.", ex);
            }
        }
        return result;
    }
}
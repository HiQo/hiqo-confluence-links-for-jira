/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.beans;

/**
 * Enumeration to define resources from confluence
 * @author alexander.kosko
 */
public enum ConfluenceResource {
    // Enum values
    PAGE("page", "Page", null, "page"),
    BLOGPOST("blogpost", "Blogpost", null, "blogpost"),
    COMMENT("comment", "Comment", null, "confluence_comment"),
    SPACE("spacedesc", "Space", null, "space"),
    USERINFO("userinfo", "User info", null, "userinfo"),
    STATUS("status", "Status", null, "status"),
    UNKNOWN("unknown", "Unknown", null, "unknown"),
    ATTACHMENT_HTML("attachment", "Attachment HTML", "html", "attachment_html"),
    ATTACHMENT_HTM("attachment", "HTML attachment", "htm", "attachment_html"),
    ATTACHMENT_GIF("attachment", "GIF attachment", "gif", "attachment_image"),
    ATTACHMENT_PNG("attachment", "PNG attachment", "png", "attachment_image"),
    ATTACHMENT_JPG("attachment", "JPG attachment", "jpg", "attachment_image"),
    ATTACHMENT_JPEG("attachment", "JPEG attachment", "jpeg", "attachment_image"),
    ATTACHMENT_BMP("attachment", "BMP attachment", "bmp", "attachment_image"),
    ATTACHMENT_JAVA("attachment", "Java attachment", "java", "attachment_java"),
    ATTACHMENT_JAR("attachment", "JAR attachment", "jar", "attachment_java"),
    ATTACHMENT_WORD("attachment", "Word attachment", "doc", "attachment_word"),
    ATTACHMENT_WORDX("attachment", "Word attachment", "docx", "attachment_word"),
    ATTACHMENT_RTF("attachment", "RTF attachment", "rtf", "attachment_rtf"),
    ATTACHMENT_EXCEL("attachment", "Excel attachment", "xls", "attachment_excel"),
    ATTACHMENT_EXCELX("attachment", "Excel attachment", "xlsx", "attachment_excel"),
    ATTACHMENT_POWERPOINT("attachment", "Powerpoint attachment", "ppt", "attachment_powerpoint"),
    ATTACHMENT_PDF("attachment", "PDF attachment", "pdf", "attachment_pdf"),
    ATTACHMENT_TEXT("attachment", "Text file attachment", "txt", "attachment_text"),
    ATTACHMENT_XML("attachment", "XML attachment", "xml", "attachment_xml"),
    ATTACHMENT_TAR("attachment", "TAR attachment", "tar", "attachment_zip"),
    ATTACHMENT_GZ("attachment", "GZ attachment", "gz", "attachment_zip"),
    ATTACHMENT_ZIP("attachment", "ZIP attachment", "zip", "attachment_zip"),
    ATTACHMENT_FILE("attachment", "Attachment", "", "attachment_text");

    // Enum properties
    private String type;
    private String name;
    private String resource;
    private String styleClass;

    /**
     * Public constructor
     * @param type
     * @param name
     * @param resource
     * @param iconPaths 
     */
    ConfluenceResource(String type, String name, String resource, String styleClass) {
        this.type = type;
        this.name = name;
        this.resource = resource;
        this.styleClass = styleClass;
    }

    /**
     * Returns confluence resource by specified type.
     * @param type
     * @return ConfluenceResource
     */
    public static ConfluenceResource getConfluenceResource(String type) {
        return getConfluenceResource(type, null);
    }

    /**
     * Returns confluence resource by specified type and resource.
     * @param type
     * @param resource
     * @return ConfluenceResource
     */
    public static ConfluenceResource getConfluenceResource(String type, String resource) {
        ConfluenceResource result = null;
        ConfluenceResource[] resourceValues = ConfluenceResource.values();
        int i = 0;
        while (result == null && i < resourceValues.length) {
            if (resourceValues[i].getType().equals(type) && (resource == null || resource.equals(resourceValues[i].getResource()))) {
                result = resourceValues[i];
            }
            i++;
        }
        if (result == null) {
            if (resource == null) {
                result = UNKNOWN;
            } else {
                result = ATTACHMENT_FILE;
            }
        }
        return result;
    }

    /**
     * Returns correct path to icon
     * @return String
     */
    public String getStyleClass() {
        return styleClass;
    }

    /**
     * Returns resource name
     * @return String
     */
    public String getResource() {
        return resource;
    }

    /**
     * Returns type name
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     * Returns name
     * @return String
     */
    public String getName() {
        return name;
    }
}
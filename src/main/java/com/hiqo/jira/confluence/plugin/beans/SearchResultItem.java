/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.beans;

/**
 * Class to store search result of confluence search
 *
 * @author alexander.kosko
 */
public class SearchResultItem {

    private String id;
    private String macroSearchId;
    private String name;
    private String url;
    private String space;
    private String lastModifiedDate = "";
    private ConfluenceResource resource;

    /**
     * Public constructor
     *
     * @param type
     * @param name
     * @param url
     */
    public SearchResultItem(String type, String name, String url) {
        this.name = name;
        this.url = url;
        this.id = new StringBuffer("name=").append(name.replaceAll("\"", "&#34")).append(";url=").append(url).append(";type=").append(type).append(";").toString();

        if ("attachment".equals(type)) {
            if (url != null) {
                String resourceType = url.substring(url.lastIndexOf(".") + 1, url.length());
                this.resource = ConfluenceResource.getConfluenceResource(type, resourceType);
            } else {
                this.resource = ConfluenceResource.ATTACHMENT_FILE;
            }
        } else {
            this.resource = ConfluenceResource.getConfluenceResource(type);
        }
    }

    /**
     * Returns id
     *
     * @return String
     */
    public String getId() {
        return id;
    }

    /**
     * Returns name
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Returns resource
     *
     * @return ConfluenceResource
     */
    public ConfluenceResource getResource() {
        return resource;
    }

    /**
     * Returns space
     *
     * @return String
     */
    public String getSpace() {
        return space;
    }

    /**
     * Sets space name
     *
     * @param space
     */
    public void setSpace(String space) {
        this.space = space;

        // Add space url to id
        addParameterToId("space", space);
    }

    /**
     * Returns URL
     *
     * @return String
     */
    public String getUrl() {
        return url;
    }

    /**
     * Returns last modified date
     *
     * @return String
     */
    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets last modified date
     *
     * @param lastModifiedDate
     */
    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    /**
     * Returns search id for macro
     *
     * @return String
     */
    public String geMacroSearchId() {
        return macroSearchId;
    }

    /**
     * Sets search id for macro
     *
     * @param macroSearchId
     */
    public void setMacroSearchId(String macroSearchId) {
        this.macroSearchId = macroSearchId;

        // Add macro search id to id
        addParameterToId("macroSearchId", macroSearchId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SearchResultItem that = (SearchResultItem) o;

        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (url != null ? !url.equals(that.url) : that.url != null) {
            return false;
        }
        if (resource == null || that.resource == null) {
            return false;
        }
        if (resource.getType() != null ? !resource.getType().equals(that.resource.getType()) : that.resource.getType() != null) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }

    private void addParameterToId(String parameterName, String parameterValue) {
        StringBuilder idSb = new StringBuilder(id);
        String paramNameWithEquals = parameterName + "=";
        int macroSearchIdIndex = id.indexOf(paramNameWithEquals, 0);
        if (macroSearchIdIndex != -1) {
            idSb.replace(macroSearchIdIndex + paramNameWithEquals.length(), idSb.indexOf(";", macroSearchIdIndex), parameterValue);
        } else {
            idSb.append(paramNameWithEquals).append(parameterValue).append(";");
        }
        id = idSb.toString();
    }
}
/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.beans;

/**
 * Class to store information about confluence space.
 * @author alexander.kosko
 */
public class ConfluenceSpaceInfo {

    private String key;
    private String name;

    /**
     * Public constructor.
     * @param key
     * @param name 
     */
    public ConfluenceSpaceInfo(String key, String name) {
        this.key = key;
        this.name = name;
    }

    /**
     * Returns space key
     * @return String
     */
    public String getKey() {
        return key;
    }

    /**
     * Returns space name
     * @return String
     */
    public String getName() {
        return name;
    }
}
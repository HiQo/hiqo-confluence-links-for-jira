/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.action;

import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.hiqo.jira.confluence.plugin.PluginConstants;
import com.hiqo.jira.confluence.plugin.beans.ConfluenceSpaceInfo;
import com.hiqo.jira.confluence.plugin.beans.SearchResultItem;
import com.hiqo.jira.confluence.plugin.beans.SearchResultItemComparator;
import com.hiqo.jira.confluence.plugin.manager.ConfluenceLinkConfigManager;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;

/**
 * Action to process user's requests from search confluence links page
 *
 * @author alexander.kosko
 */
public class ConfluencePageSearchAction extends AbstractConfluenceRefAction implements PluginConstants {

    private Logger logger = Logger.getLogger(ConfluencePageSearchAction.class);
    private static final long serialVersionUID = 6205729805262670148L;
    private final String SEARCH_RESULTS_SESSION_PARAMETER = "SEARCH_RESULTS_SESSION_PARAMETER";
    private final String ACTION_PARAMETER_SEARCH = "search";
    private final String ACTION_PARAMETER_SORT = "sort";
    public List<ConfluenceSpaceInfo> spaceList;
    private String linksFieldId;
    private String searchParamQuery = "";
    private String searchParamSpace = "";
    private String processAction;
    private String sortColumn = "name";
    private boolean ascending = true;

    /**
     * Public constructor
     *
     * @param trustedApplicationsManager
     */
    public ConfluencePageSearchAction(TrustedApplicationsManager trustedApplicationsManager) {
        super(trustedApplicationsManager);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String doExecute() throws Exception {
        String result = super.doExecute();
        if (getErrorMessages() == null || getErrorMessages().isEmpty()) {
            this.spaceList = getSpaceListFromConfluence("plugin.text.all-spaces");
            if (ACTION_PARAMETER_SEARCH.equals(processAction)) {
                initSearchResults();
                if (searchParamQuery.length() >= 1) {
                    Object searchResults = xmlRpcHandler.execute("confluence1.search", initSearchParams(searchParamQuery.replaceAll("\"", "\\\\\""), searchParamSpace));
                    if (searchResults instanceof List) {
                        for (Map<String, String> searchResult : ((List<Map<String, String>>) searchResults)) {
                            SearchResultItem item = new SearchResultItem(searchResult.get("type"), searchResult.get("title"), searchResult.get("url"));
                            if (!getSearchResults().contains(item)) {
                                fillInfoById(searchResult.get("id"), item);
                                getSearchResults().add(item);
                            }
                        }
                    } else if (searchResults instanceof XmlRpcException) {
                        addErrorMessage(((XmlRpcException) searchResults).getMessage());
                        logger.error("An exception has been occurred till search processing.", (XmlRpcException) searchResults);
                    } else {
                        addErrorMessage(getText("plugin.error.search.no-results"));
                    }
                } else {
                    addErrorMessage(getText("plugin.error.search.empty-query"));
                }
            } else if (!ACTION_PARAMETER_SORT.equals(processAction)) {
                initSearchResults();
            }
            if (getSearchResults() == null) {
                addErrorMessage(getText("plugin.error.session.expired"));
            } else {
                Collections.sort(getSearchResults(), new SearchResultItemComparator(getSortColumn(), isAscending()));
            }
        }
        return result;
    }

    /**
     * Returns the list of spaces
     *
     * @return List<ConfluenceSpaceInfo>
     */
    public List<ConfluenceSpaceInfo> getSpaceList() {
        return this.spaceList;
    }

    /**
     * Returns search query parameter value
     *
     * @return String
     */
    public String getSearchParamQuery() {
        return searchParamQuery;
    }

    /**
     * Sets search query parameter value
     *
     * @param searchParamQuery
     */
    public void setSearchParamQuery(String searchParamQuery) {
        this.searchParamQuery = searchParamQuery;
    }

    /**
     * Returns search space parameter value
     *
     * @return String
     */
    public String getSearchParamSpace() {
        return searchParamSpace;
    }

    /**
     * Sets search space parameter value
     *
     * @param searchParamSpace
     */
    public void setSearchParamSpace(String searchParamSpace) {
        this.searchParamSpace = searchParamSpace;
    }

    /**
     * Returns the id of the custom field which contains confluence links
     *
     * @return String
     */
    public String getLinksFieldId() {
        return linksFieldId;
    }

    /**
     * Sets the id of the custom field which contains confluence links
     *
     * @param linksFieldId
     */
    public void setLinksFieldId(String linksFieldId) {
        this.linksFieldId = linksFieldId;
    }

    /**
     * Sets the action processed on the search screen
     *
     * @param processAction
     */
    public void setProcessAction(String processAction) {
        this.processAction = processAction;
    }

    /**
     * Returns the list of items found by search in confluence
     *
     * @return List<SearchResultItem>
     */
    public List<SearchResultItem> getSearchResults() {
        return (List<SearchResultItem>) request.getSession().getAttribute(SEARCH_RESULTS_SESSION_PARAMETER);
    }

    /**
     * Returns sort order for search results
     *
     * @return boolean
     */
    public boolean isAscending() {
        return ascending;
    }

    /**
     * Sets sort order for search results
     *
     * @param ascending
     */
    public void setAscending(boolean ascending) {
        this.ascending = ascending;
    }

    /**
     * Returns sort column for search results
     *
     * @return String
     */
    public String getSortColumn() {
        return sortColumn;
    }

    /**
     * Sets sort column for search results
     *
     * @param sortColumn
     */
    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    /**
     * Returns file name of the picture which should be displayed according to ascending order
     *
     * @return String
     */
    public String getSortImage() {
        if (ascending) {
            return "icon_sortascending.png";
        } else {
            return "icon_sortdescending.png";
        }
    }

    private Vector initSearchParams(String query, String space) {
        Vector params = new Vector();
        params.add("");
        params.add(query);
        if (space != null) {
            Hashtable searchParameters = new Hashtable();
            searchParameters.put("spaceKey", space);
            params.add(searchParameters);
        }
        params.add(new Integer(getMaxSearchResults()));
        return params;
    }

    private void fillInfoById(String resourceId, SearchResultItem item) throws Exception {
        if (item != null && item.getResource() != null) {
            String pageId = resourceId;
            String execString = "";
            String spaceKey = "";
            Date modifiedDate = null;
            if ("attachment".equals(item.getResource().getType())) {
                pageId = getParameterValueFromUrl(item.getUrl(), "pageId");
                execString = "confluence2.getPage";
                modifiedDate = getAttachmentDate(pageId, item.getName());
            } else {
                switch (item.getResource()) {
                    case PAGE:
                        execString = "confluence2.getPage";
                        break;
                    case BLOGPOST:
                        execString = "confluence2.getBlogEntry";
                        break;
                    case COMMENT:
                        pageId = getPageIdFromComment(resourceId);
                        execString = "confluence2.getPage";
                        modifiedDate = getElementDate(resourceId, "confluence2.getComment", "modified");
                        break;
                    case STATUS:
                        item.setSpace(getText("plugin.text.status"));
                        break;
                    case SPACE:
                        item.setSpace(item.getName());
                        break;
                    case USERINFO:
                        item.setSpace(getText("plugin.text.user-info"));
                        modifiedDate = getUserDate(item.getUrl());
                        break;
                    case UNKNOWN:
                        item.setSpace(getText("plugin.text.not-available"));
                        break;
                }
            }
            if (!"".equals(execString) && !"".equals(pageId)) {
                Vector pageParams = new Vector();
                pageParams.add("");
                pageParams.add(pageId);
                Object entity = xmlRpcHandler.execute(execString, pageParams);
                if (entity instanceof XmlRpcException) {
                    entity = xmlRpcHandler.execute("confluence2.getBlogEntry", pageParams);
                }
                if (entity instanceof Map) {
                    Map<String, Object> page = (Map<String, Object>) entity;
                    if (modifiedDate == null) {
                        modifiedDate = (page.get("modified") != null) ? (Date) page.get("modified") : (Date) page.get("publishDate");
                    }
                    spaceKey = (String) page.get("space");
                    Vector spaceParams = new Vector();
                    spaceParams.add("");
                    spaceParams.add(spaceKey);
                    Object space = xmlRpcHandler.execute("confluence2.getSpace", spaceParams);
                    item.setSpace(((Map<String, String>) space).get("name"));
                } else {
                    item.setSpace(getText("plugin.text.not-available"));
                }
            }
            if (modifiedDate != null) {
                item.setLastModifiedDate(sdf.format(modifiedDate));
            } else {
                item.setLastModifiedDate(getText("plugin.text.not-available"));
            }
            item.setMacroSearchId(pageId);
        }
    }

    private String getParameterValueFromUrl(String url, String parameter) throws Exception {
        if (url == null || parameter == null) {
            return "";
        }
        String result = "";
        if (url != null && url.indexOf(parameter + "=") >= 0) {
            int beginIndex = url.indexOf(parameter + "=") + parameter.length() + 1;
            int endIndex = url.indexOf("&", beginIndex);
            if (endIndex < 0) {
                endIndex = url.length();
            }
            result = url.substring(beginIndex, endIndex);
        }
        return result;
    }

    private String getPageIdFromComment(String commentId) throws Exception {
        Vector params = new Vector();
        params.add("");
        params.add(commentId);
        Object response = xmlRpcHandler.execute("confluence2.getComment", params);
        if (response instanceof Map) {
            Map<String, Object> attachment = (Map<String, Object>)response;
            return (String) attachment.get("pageId");
        } else {
            return "";
        }
    }

    private Date getElementDate(String id, String execString, String propertyName) throws Exception {
        Vector params = new Vector();
        params.add("");
        params.add(id);
        Map<String, Object> element = (Map<String, Object>) xmlRpcHandler.execute(execString, params);
        return (Date) element.get(propertyName);
    }

    private Date getAttachmentDate(String id, String fileName) throws Exception {
        Vector params = new Vector();
        params.add("");
        params.add(id);
        params.add(fileName);
        params.add("0");
        Map<String, String> element = (Map<String, String>) xmlRpcHandler.execute("confluence2.getAttachment", params);
        String dateInMilis = getParameterValueFromUrl(element.get("url"), "modificationDate");
        return new Date(Long.valueOf(dateInMilis).longValue());
    }

    private Date getUserDate(String url) throws Exception {
        if (url == null) {
            return null;
        }
        String userName = url.substring(url.lastIndexOf("~") + 1, url.length());
        Vector params = new Vector();
        params.add("");
        params.add(userName);
        Map<String, Object> element = (Map<String, Object>) xmlRpcHandler.execute("confluence2.getUserInformation", params);
        return (Date) element.get("lastModificationDate");
    }

    private int getMaxSearchResults() {
        return (ConfluenceLinkConfigManager.getInstance().getMaxSearchResultsCount() > 0)
                ? ConfluenceLinkConfigManager.getInstance().getMaxSearchResultsCount() : DEFAULT_MAX_SEARCH_RESULTS;
    }

    private void initSearchResults() {
        request.getSession().setAttribute(SEARCH_RESULTS_SESSION_PARAMETER, new ArrayList<SearchResultItem>());
    }
}
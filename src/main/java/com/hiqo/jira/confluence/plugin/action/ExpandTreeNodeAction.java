/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.action;

import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.ServletOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import webwork.action.ActionContext;

/**
 * Action to process AJAX requests to expand space page's tree node
 *
 * @author alexander.kosko
 */
public class ExpandTreeNodeAction extends AbstractConfluenceRefAction {

    private static final long serialVersionUID = 2732728432588140679L;
    private Logger logger = Logger.getLogger(ExpandTreeNodeAction.class);
    private String pageId;

    /**
     * Public constructor
     *
     * @param trustedApplicationsManager
     */
    public ExpandTreeNodeAction(TrustedApplicationsManager trustedApplicationsManager) {
        super(trustedApplicationsManager);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String doExecute() throws Exception {
        List<String> childrenList = new ArrayList<String>();
        try {
            Vector params = new Vector<String>();
            params.add("");
            params.add(pageId);
            Object nodeChildren = xmlRpcHandler.execute("confluence1.getChildren", params);
            if (nodeChildren instanceof List) {
                for (Map<String, String> child : (List<Map<String, String>>) nodeChildren) {
                    childrenList.add(new StringBuffer("").append("{").append("\"id\":" + child.get("id")).append(",\"title\":\"").append(
                            child.get("title").replaceAll("\"", "&#34")).append("\",\"isFolder\":").append(hasChildren(child.get("id")) ? "1" : "0").append(
                            ",\"url\":\"").append(child.get("url")).append("\"}").toString());
                }
            } else if (nodeChildren instanceof XmlRpcException) {
                addErrorMessage(((XmlRpcException) nodeChildren).getMessage());
                logger.error("An exception has been occurred till getting space list.", (XmlRpcException) nodeChildren);
            }
        } catch (Exception ex) {
            addErrorMessage(ex.getMessage());
        }

        StringBuffer jsonResponse = new StringBuffer().append("[").append(StringUtils.join(childrenList, ",")).append("]");
        ServletOutputStream out = ActionContext.getResponse().getOutputStream();
        out.write(jsonResponse.toString().getBytes("UTF-8"));
        out.close();
        return SUCCESS;
    }

    /**
     * Sets page id request parameter
     *
     * @param pageId
     */
    public void setPageId(String pageId) {
        this.pageId = pageId;
    }
}
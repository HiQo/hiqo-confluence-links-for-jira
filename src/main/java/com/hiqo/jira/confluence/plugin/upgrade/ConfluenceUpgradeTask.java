/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.upgrade;

import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import java.util.Collection;

/**
 * Class to process plugin's upgrade tasks
 * @author alexander.kosko
 */
public class ConfluenceUpgradeTask implements PluginUpgradeTask {

    /**
     * {@inheritDoc}
     */
    @Override
    public int getBuildNumber() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getShortDescription() {
        return "Upgrade Confluence Link plugin to the new version";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Message> doUpgrade() throws Exception {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPluginKey() {
        return "com.hiqo.jira.confluence-integrator-plugin";
    }
}
/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.manager;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to store confluence link configuration
 * @author alexander.kosko
 */
public class ConfluenceLinkConfigManager {

    private final String CONFLUENCE_URL_PROPERTY = "CONFLUENCE_URL_PROPERTY";
    private final String CONFLUENCE_MAX_SEARCH_RESULTS_PROPERTY = "CONFLUENCE_MAX_SEARCH_RESULTS_PROPERTY";
    private final String CONFLUENCE_PROPERTYSET_INSTANCE = "ofbiz";
    private final Long CONFLUENCE_PROPERTY_ENTITY_ID = new Long(100500);
    private final String CONFLUENCE_PROPERTY_ENTITY_NAME = "com.hiqo.jira.confluence.plugin";
    private final String CONFLUENCE_PROPERTY_DELEGATOR_NAME = "default";
    private static ConfluenceLinkConfigManager manager;
    private PropertySet confluenceProrertySet;

    /**
     * Public constructor
     */
    private ConfluenceLinkConfigManager() {
        super();
        Map<String, Object> confluenceArguments = new HashMap<String, Object>();
        confluenceArguments.put("entityId", CONFLUENCE_PROPERTY_ENTITY_ID);
        confluenceArguments.put("entityName", CONFLUENCE_PROPERTY_ENTITY_NAME);
        confluenceArguments.put("delegator.name", CONFLUENCE_PROPERTY_DELEGATOR_NAME);
        confluenceProrertySet = PropertySetManager.getInstance(CONFLUENCE_PROPERTYSET_INSTANCE, confluenceArguments);
    }

    /**
     * Returns an existing instance of ConfluenceLinkConfigManager or creates new
     * @return ConfluenceLinkConfigManager
     */
    public static ConfluenceLinkConfigManager getInstance() {
        if (manager == null) {
            manager = new ConfluenceLinkConfigManager();
        }
        return manager;
    }

    /**
     * Returns confluence URL from properties
     * @return String
     */
    public String getConfluenceUrl() {
        String result = (confluenceProrertySet.getString(CONFLUENCE_URL_PROPERTY) != null) ? confluenceProrertySet.getString(CONFLUENCE_URL_PROPERTY) : "";
        if (!"".equals(result.trim()) && !result.endsWith("/")) {
            result += "/";
        }
        return result;
    }

    /**
     * Sets confluence URL to properties
     * @param url 
     */
    public void setConfluenceUrl(String url) {
        confluenceProrertySet.setString(CONFLUENCE_URL_PROPERTY, url);
    }

    /**
     * Returns maximum number of search records
     * @return integer
     */
    public int getMaxSearchResultsCount() {
        return confluenceProrertySet.getInt(CONFLUENCE_MAX_SEARCH_RESULTS_PROPERTY);
    }

    /**
     * Sets maximum number of search records
     * @param count 
     */
    public void setMaxSearchResultsCount(int count) {
        confluenceProrertySet.setInt(CONFLUENCE_MAX_SEARCH_RESULTS_PROPERTY, count);
    }
}